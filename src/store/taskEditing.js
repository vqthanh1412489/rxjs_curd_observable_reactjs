import {ActionTypes} from '../ActionTypes';

import {Subject} from 'rxjs';

const initialState = {
    taskEditing: {},
};

var state = initialState;

export const storeEditting = new Subject();
export const eventDispatcherEditing = new Subject();
 
eventDispatcherEditing.subscribe((action) => {
    switch(action.type){
        case ActionTypes.SET_TASKEDITING:
            state.taskEditing = action.payload;
            storeEditting.next(state);
            break;
        case ActionTypes.GET_TASKEDITING:
                storeEditting.next(state);
                break;
        default: return;
    }
});