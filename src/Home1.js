import React, { useState, useEffect } from 'react';
import { eventDispatcher, store } from './services1';
import { ActionTypes } from './ActionTypes';

function Home1(){
    const [users, setUsers] = useState([]);
    const [user, setUser] = useState({});

    useEffect(() => {
        eventDispatcher.next({type: ActionTypes.GET_NOTES});   
    }, [users])

    store.subscribe(a => {
        setUsers(a.users)}); 
    const showUsers = users.map((user, index) => {
        return(
            <p key={index}>
                {user.name}
            </p>
        );
    });

    const handleChange = e => {
        const { name, value } = e.target;
        setUser({ ...user, [name]: value });
    }

    const handleSubmit = e => {
        e.preventDefault();
        eventDispatcher.next({ type: ActionTypes.CREATE_NOTE, payload: user })
    }

    return(
        <div>
            {showUsers}
            <form onSubmit={handleSubmit} className="formUpdate">
            <div className="form-group">
                <input type="text" className="form-control" placeholder="Enter new username"
                    name="username"
                    value={user.username}
                    onChange={handleChange}
                />
            </div>
            <div className="form-group" o>
                <input type="text" className="form-control" placeholder="Enter new name"
                    name="name"
                    value={user.name}
                    onChange={handleChange}
                />
            </div>
            <button type="submit" className="btn btn-primary">Add</button>
        </form>
        </div>
    );
}

export default Home1;