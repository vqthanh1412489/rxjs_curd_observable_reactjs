import React, { useState } from 'react';
import { eventDispatcher } from '../services1';
import { ActionTypes } from '../ActionTypes';
import { Observable } from 'rxjs';

function FormAddUser(){
    const [user, setUser] = useState({});
    const handleChange = e => {
        const { name, value } = e.target;
        setUser({ ...user, [name]: value });
    }

    const handleSubmit = e => {
        e.preventDefault();
        eventDispatcher.next({ type: ActionTypes.CREATE_NOTE, payload: user })
    }

    return(
        <form onSubmit={e => handleSubmit(e)} className="formAdd">
            <div className="form-group">
                <input type="text" className="form-control" placeholder="Enter username"
                    name="username"
                    value={user.username}
                    onChange={e => handleChange(e)}
                />
                <small className="form-text text-muted">Please Input Username</small>
            </div>
            <div className="form-group">
                <input type="text" className="form-control" placeholder="Entar name"
                    name="name"
                    value={user.name}
                    onChange={e => handleChange(e)}
                />
            </div>
            <button type="submit" className="btn btn-primary">Add</button>
        </form>
    );
}

export default FormAddUser;