import React, { useState, useEffect } from 'react'
import { eventDispatcher, store } from '../services1';
import { eventDispatcherEditing } from '../store/taskEditing';
import { ActionTypes } from '../ActionTypes';

import ItemUser from './ItemUser';

function ListUsers(){
    const [users, setUsers] = useState([]);

    useEffect(() => {
        eventDispatcher.next({type: ActionTypes.GET_NOTES});   
    }, [users])
    store.subscribe(a => {
        setUsers(a.users)
    }); 
    const onDelUser = e => {
        eventDispatcher.next({type: ActionTypes.DELETE_NOTE, payload: e});           
    }
    const onGetItem = e => {
        eventDispatcherEditing.next({type: ActionTypes.SET_TASKEDITING, payload: e});        
    }
    return(
        <table className="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Username</th>
                    <th scope="col">Name</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                {
                    users.length > 0 ? (
                        users.map((user, index) => {
                            return (
                                <ItemUser user={user} key={index} 
                                onDelUser={onDelUser}
                                onGetItem={onGetItem}
                                />       
                            );
                        })
                    ) : (
                            <tr>
                                <td colSpan={3}>No users</td>
                            </tr>
                        )
                }

            </tbody>
        </table>
    );
}  

export default ListUsers;