import React, { useState, useEffect } from 'react';
import { storeEditting } from '../store/taskEditing';
import { eventDispatcher } from '../services1';
import { ActionTypes } from '../ActionTypes';

function FormEditUser(){
    const [user, setUser] = useState({});
    const [taskEditing, setTaskediting] = useState({});

    const handleChange = e => {
        const { name, value } = e.target;
        setUser({ ...user, [name]: value });
    }

    useEffect(() => {
        storeEditting.subscribe(task => {
            setTaskediting(task.taskEditing)});
        setUser({ ...user, username: taskEditing.username, name: taskEditing.name, id: taskEditing.id });
    }, [taskEditing])

    const handleSubmit = e => {
        e.preventDefault();
        eventDispatcher.next({ type: ActionTypes.UPDATE_NOTE, payload: user })
        setUser({
            name: '',
            username: ''
        });
    }


    return(
        <form onSubmit={e => handleSubmit(e)} className="formAdd">
            <div className="form-group">
                <input type="text" className="form-control" placeholder="Enter username"
                    name="username"
                    value={user.username}
                    onChange={e => handleChange(e)}
                />
                <small className="form-text text-muted">Please Input Username</small>
            </div>
            <div className="form-group">
                <input type="text" className="form-control" placeholder="Entar name"
                    name="name"
                    value={user.name}
                    onChange={e => handleChange(e)}
                />
            </div>
            <button type="submit" className="btn btn-primary">Update</button>
        </form>
    );
}

export default FormEditUser;