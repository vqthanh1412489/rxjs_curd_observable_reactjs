import React, { useState, useEffect } from 'react';
import ListUsers from './ListUsers';
import FormAddUser from './FormAddUser';
import FormEditUser from './FormEditUser';
import { storeEditting, eventDispatcherEditing } from '../store/taskEditing';
import { ActionTypes } from '../ActionTypes';


function isEmpty(obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            return false;
        }
    }

    return JSON.stringify(obj) === JSON.stringify({});
}

function Home(){
    const [taskEditing, setTaskediting] = useState({});
    useEffect(() => {
        eventDispatcherEditing.next({type: ActionTypes.GET_TASKEDITING});   
        storeEditting.subscribe(task => setTaskediting(task));
    }, [taskEditing])
    return(
        <div>
            <div className="container">
                <h1>CRUD App with Hooks - RxJS</h1>
                <div className="row">
                    <div className="col-md-6">
                        {/* <button className={btnAddUserStyle} onClick={() => props.onSetTaskEditting()}>Add User</button> */}
                        <h2>{isEmpty(taskEditing) ? 'Add User' : 'Edit user'}</h2>
                        {isEmpty(taskEditing) ? <FormAddUser /> : <FormEditUser />}

                    </div>
                    <div className="col-md-6">
                        <h2>View users</h2>
                        <ListUsers />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Home;