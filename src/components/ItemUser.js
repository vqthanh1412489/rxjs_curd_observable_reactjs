import React from 'react';

function ItemUser(props){
    const { user } = props;
    const onDelUser = () => {
        props.onDelUser(user.id);
    }
    const onGetItem = () => {
        props.onGetItem(user);
    }
    return(
        
        <tr key={user.id}>
                                    <th scope="row">{user.id}</th>
                                    <td>{user.username}</td>
                                    <td>{user.name}</td>
                                    <td>
                                        <button className="btn btn-info"
                                            onClick={onGetItem}
                                        >Update</button>
                                        <button className="btn btn-danger"
                                            onClick={onDelUser}
                                        >Delete</button>
                                    </td>
                                </tr>
    );
}

export default ItemUser;