import React, { useState, useEffect } from 'react';
import { UserService } from './services';

function Home(){
    const [users, setUsers] = useState([]);
    const [user, setUser] = useState({});
    const userService = new UserService();
    useEffect(() => {
        userService.getAllUser();
        userService.users$.subscribe(users => setUsers(users));
    }, []);

    const showUsers = users.map((user, index) => {
        return(
            <p key={index}>
                {user.name}
            </p>
        );
    });

    const handleChange = e => {
        const { name, value } = e.target;
        setUser({ ...user, [name]: value });
    }

    const handleSubmit = e => {
        e.preventDefault();
        userService.addUser(user);
        userService.users$.subscribe(users => setUsers(users));
    }

    return(
        <div>
            {showUsers}
            <div>
            <form onSubmit={handleSubmit} className="formUpdate">
            <div className="form-group">
                <input type="text" className="form-control" placeholder="Enter new username"
                    name="username"
                    value={user.username}
                    onChange={handleChange}
                />
            </div>
            <div className="form-group" o>
                <input type="text" className="form-control" placeholder="Enter new name"
                    name="name"
                    value={user.name}
                    onChange={handleChange}
                />
            </div>
            <button type="submit" className="btn btn-primary">Add</button>
        </form>
            </div>
        </div>

    );
}

export default Home;