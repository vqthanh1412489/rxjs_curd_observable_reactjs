export const ActionTypes = {
    CREATE_NOTE: '[HOME] Create a note',
    DELETE_NOTE: '[HOME] Delete a note',
    UPDATE_NOTE: '[HOME] Update a note',
    GET_NOTES: '[HOME] Get all notes',
    SET_TASKEDITING: '[HOME] Set TaskEditing',
    GET_TASKEDITING: '[HOME] Get TaskEditing',
  }