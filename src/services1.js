import {ActionTypes} from './ActionTypes';

import {Subject} from 'rxjs';
import { fetchUsers, addUser, delUser, updateUser } from './services/UserServices'

const initialState = {
    users: [],
};

var state = initialState;

export const store = new Subject();
export const eventDispatcher = new Subject();

function findIndexById(arr, id) {
    var index = -1;
    for (let i = 0; i < arr.length; i++) {
        if (arr[i].id === id) index = i;
    }
    return index;
}

eventDispatcher.subscribe((action) => {
    switch(action.type){
        case ActionTypes.GET_NOTES:
            fetchUsers()
            .then(res => {
                if (res){
                    state.users = res.data;
                    store.next(state);
                }
            })
            .catch(err => console.log(err));
            break;
        case ActionTypes.CREATE_NOTE:
                addUser(action.payload)
                .then(res => {
                    if (res){
                        state = {
                            users: [...state.users, res.data],
                        };
                        store.next(state);
                    }
                })
                .catch(err => console.log(err));
                break;
        case ActionTypes.DELETE_NOTE:
                delUser(action.payload)
                .then(res => {
                    if (res){
                        var index = findIndexById(state, action.payload);
                        state.splice(index, 1);
                        store.next(state);
                    }
                })
                .catch(err => console.log(err));
                break;
        case ActionTypes.UPDATE_NOTE:
                var { id, name, username } = action.payload;
                updateUser(id, { name, username })
                .then(res => {
                    if (res){
                        var index = findIndexById(state, id);
                        state[index] = {
                            ...state[index],
                            name: res.data.name,
                            username: res.data.username
                        }
                        store.next(state);
                        
                    }
                })
                .catch(err => console.log(err));
                break;
        default: return;
    }
});