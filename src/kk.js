import { BehaviorSubject } from 'rxjs'; 

import axios from 'axios';


export class UserService {
    constructor(){
        this.emitter = new BehaviorSubject([]);
        this.url = 'http://5ce3ab21e7bf4100144c66c4.mockapi.io'
    }

    getUserAll = () => {
        axios.get(`${this.url}/users`)
        .then(res => {
            this.emitter.next(res.data);
        })
        .catch(err => this.emitter.error(err));
    }

    addUser = user => {
        
        axios.post(`${this.url}/users`, {
            name: user.name,
            username: user.username
        })
        .then(res => {
            
        })
        .catch(err => this.emitter.error(err));        
    }
}

